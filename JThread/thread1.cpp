﻿#include "thread1.h"
#include <QDebug>
#include <QDateTime>
#include <QMutexLocker>

Thread1::Thread1(QObject *parent) : QObject(parent)
{
    IsStop = false;
}


void Thread1::doWork()
{
    IsStop = false;
    while(1) {
        QMutexLocker locker(&StopMutex);
        if(IsStop)
            break;
        qDebug() << this->thread()->currentThreadId() << ": " << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
        QThread::sleep(1);
    }
    qDebug() << "Thread end...";
}

void Thread1::Stop()
{
    QMutexLocker locker(&StopMutex);
    IsStop = true;
    qDebug() << "Stop Thread: " << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
}
