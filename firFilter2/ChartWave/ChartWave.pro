#-------------------------------------------------
#
# Project created by QtCreator 2020-09-02T10:59:45
#
#-------------------------------------------------

QT       += core gui printsupport

TARGET   =  ChartWave
TEMPLATE =  lib

DEFINES += CHARTWAVE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG(debug, debug | release) {
    LIBS    += -L$$PWD/../lib_debug -lAlgorithm

    DESTDIR  += $$PWD/../lib_debug
} else {
    LIBS    += -L$$PWD/../lib_release -lAlgorithm

    DESTDIR  += $$PWD/../lib_release
}

SOURCES += \
        chartwave.cpp \
    qcustomplot.cpp

HEADERS += \
        chartwave.h \
        chartwave_global.h \ 
    qcustomplot.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS += \
    chartwave.ui
